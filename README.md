# T-POO-700-BDX-4

This is a pool project for EPITECH Bordeaux.

<div align="center">

# TimeManager - API

<br />
<p> TimeManager ✨</p>
</div>

## Installation
### Pre-requisites
- [Docker](https://docs.docker.com/get-docker/)

### Launch the docker
```bash
# Before all launch docker in you computer
```

### run the command
check that you are in `~/<repos>` and you can see the folder /api and /front
```bash
# launch docker compose

$ docker-compose up --build
```

## Verify you have access to the frontend [here](http://localhost:8080/)
