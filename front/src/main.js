import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
// import store from "./store";
import VueSplide from '@splidejs/vue-splide';
import 'vue3-timepicker/dist/VueTimepicker.css';

createApp(App).use(router).use(VueSplide ).mount('#app')