import {callBackEnd} from '../context/User'
import jwt from 'jsonwebtoken'

const Role = {
    Admin: 'Admin',
    Collaborateur: 'Collaborateur',
    Manager: 'Manager',
    RH: 'RH',
}

const getRoleUser = async () => {
    const token = sessionStorage.getItem("token")
    var decoded = jwt.decode(token);

    const user = await callBackEnd.getUser(decoded.sub)

    let result = undefined

    if(user.role === "Admin"){
        result = Role.Admin
    }
    else if (user.role === "Collaborateur"){
        result = Role.Collaborateur
    }
    else if (user.role === "Manager"){
        result = Role.Manager
    }
    else if (user.role === "RH"){
        result = Role.RH
    }

    return result
}

export const getUserRole = {
    Role,
	getRoleUser
}