import { createRouter, createWebHistory } from 'vue-router'
// import Home from '../components/Home'
import Dashboard from '../components/Dashboard/Dashboard'
import Login from '../components/Auth/Login.vue'
import Register from '../components/Auth/Register'
import User from '../components/User/User.vue'
import Admin from '../components/Admin/Index.vue'
import WorkingTime from '../components/WorkingTime/WorkingTime'
import NotFound from '../components/NotFound'
import Teams from '../components/Teams/Index.vue'
import {getUserRole} from '../_helper/Roles'

import {verif} from '../services/isTokenValid'

const routes = [
    {
        path: '/',
        name: 'home',
        component: Login
    },
    {
        path: '/login',
        name: 'Login',
        component: Login
    },
    {
        path: '/Dashboard',
        name: 'Dashboard',
        component: Dashboard,
        meta: { authorize: [getUserRole.Role.Collaborateur, getUserRole.Role.Admin, getUserRole.Role.Manager, getUserRole.Role.RH] }
    },
    {
        path: '/register',
        name: 'Register',
        component: Register
    },
    {
        path: '/workingtime',
        name: 'workingtime',
        component: WorkingTime,
        meta: { authorize: [getUserRole.Role.Collaborateur, getUserRole.Role.Admin, getUserRole.Role.Manager, getUserRole.Role.RH] }
    },
    {
        path: '/admin',
        name: 'admin',
        component: Admin,
        meta: { authorize: [getUserRole.Role.Admin] }
    },
    {
        path: '/teams',
        name: 'teams',
        component: Teams,
        meta: { authorize: [getUserRole.Role.Admin] }
    },
    {
        path: '/user',
        name: 'user',
        component: User,
        meta: { authorize: [getUserRole.Role.Collaborateur, getUserRole.Role.Admin, getUserRole.Role.Manager, getUserRole.Role.RH] }
    },
    {
        path: '/:pathMatch(.*)*',
        name: '404',
        component: NotFound
    }

]

const router = createRouter({ history: createWebHistory(), routes })

router.beforeEach(async (to, from, next) => {
    if(to.path === "/login" || to.path === "/register" || to.path === "/")
        return next()

    if(sessionStorage.getItem("token") === null || !verif.isTokenExpired()){
        return next({ path: '/login' });
    }

    const userRole = await getUserRole.getRoleUser()

    // redirect to login page if not logged in and trying to access a restricted page
    const { authorize } = to.meta;
    const currentUser = userRole;

    if (authorize) {
        if (!currentUser) {
            // not logged in so redirect to login page with the return url
            return next({ path: '/login', query: { returnUrl: to.path } });
        }

        // check if route is restricted by role
        if (authorize.length && !authorize.includes(userRole)) {
            // role not authorised so redirect to home page
            return next({ path: '/' });
        }
    }

    next();
})

export default router