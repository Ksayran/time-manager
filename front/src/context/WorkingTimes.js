import {
    deleteWorkingTimeById,
    postWorkingTimeByUserId, putWorkingTime
} from './Config'

import axios from 'axios';
import jwt from "jsonwebtoken";

const token = sessionStorage.getItem("token")
var decoded = jwt.decode(token);

const getWorkingTimes = async (UserId) => {

    let result = []
    if (UserId == -1) {
        UserId = decoded.sub
    }
    const config = {
        method: 'get',
        url: postWorkingTimeByUserId + UserId + "?start=2021-10-01 18:35:54&end=2021-10-31 18:50:55",
        headers: {
            'Authorization': "Bearer " + sessionStorage.getItem("token"),
            'Content-Type': 'application/json'
        }
    }

    await axios(config)
        .then(response => {
            result = response.data.data
        })
        .catch(error => {
            console.log(error)
        });

    return result
}

const postWorkingTimes = async (start, end, UserId) => {
    let result = []


    if (UserId == -1) {
        UserId = decoded.sub
    }
    var data = JSON.stringify({
        "workingtimes": {
            "end": end,
            "start": start
        }
    });
    var config = {
        method: 'post',
        url: postWorkingTimeByUserId + UserId,
        headers: {
            'Authorization': "Bearer " + sessionStorage.getItem("token"),
            'Content-Type': 'application/json'
        },
        data : data
    };

    await axios(config)
        .then(response => {
            localStorage.setItem('workingTimes', JSON.stringify(response.data.data));
            if (localStorage.user)
                result = JSON.stringify(localStorage.workingTimes);
            else {
                result = response.data.data
            }
        })
        .catch(error => {
            console.log(error)
        });

    return result
}

const deleateWorkingTimes = async (id) => {
    let result = []

    var config = {
        method: 'delete',
        url: deleteWorkingTimeById + id,
        headers: {
            'Authorization': "Bearer " + sessionStorage.getItem("token"),
            'Content-Type': 'application/json'
        },
    };
    await axios(config)
        .then(response => {
            result = response.data.data
        })
        .catch(error => {
            console.log(error)
        });

    return result
}

const putWorkingTimes = async (id, end, start) => {
    let result = []

    const config = {
        method: 'put',
        url: putWorkingTime + id,
        headers: {
            'Authorization': "Bearer " + sessionStorage.getItem("token"),
            'Content-Type': 'application/json'
        },
        data: JSON.stringify({
            "workingtimes": {
                "user": decoded.sub,
                "end": end,
                "start": start
            }
        })
    };

    await axios(config)
        .then(response => {
            result = response.data
        })
        .catch(error => {
            console.log(error)
        });

    return result
}

export const callBackEndCalendar = {
    getWorkingTimes,
    putWorkingTimes,
    postWorkingTimes,
    deleateWorkingTimes,
}
