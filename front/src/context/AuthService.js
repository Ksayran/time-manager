import axios from 'axios';
import {
    getApiLogin,
    getApiRegister,
	getLogoutUser
} from './Config'


const login = async (email, password) => {
	let result

	const config = {
		method: 'post',
		url: getApiLogin,
		headers: {
			'Content-Type': 'application/json'
		},
		data: JSON.stringify({
			"email": email,
			"password": password
		})
	}

	await axios(config)
	.then(response => {
		localStorage.setItem('user', JSON.stringify(result));
		result = response.data.token
    })
	.catch(error => {
		console.log(error.response.statusText)
	});

	return result
}

const register = async (username, email, password, lastname, firstname) => {
	let result = []

	const config = {
		method: 'post',
		url: getApiRegister,
		headers: {
			'Content-Type': 'application/json'
		},
		data: JSON.stringify({
			"username": username,
			"email": email,
			"password": password,
			"lastname": lastname,
			"firstname": firstname
		})
	}

	await axios(config)
	.then(response => {
		result = response.status === 201 ? true : false
	})
	.catch(error => {
		console.log(error)
	})

	return result
}

const logout = async () => {
	let result

	const config = {
		method: 'post',
		url: getLogoutUser,
		headers: {
			'Authorization': "Bearer " + sessionStorage.getItem("token"),
			'Content-Type': 'application/json'
		},
		data: JSON.stringify({
			"token": sessionStorage.getItem("token")
		})
	}

	await axios(config)
	.then(response => { 
		result = response.status === 200 ? true : false
    })
	.catch(error => {
		console.log(error.response.statusText)
	});

	return result
}

export const callBackEndAuth = {
	login,
	register,
	logout
}
