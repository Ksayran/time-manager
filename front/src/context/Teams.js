import { 
	getAllTeamsRoute,
	getAllUserByManagerIdRoute,
  postAddNewUserToTeamRoute,
  deleteUserToTeamRoute
} from './Config'

import axios from 'axios';


export const getAllTeams = async () =>{
    let result = []
  
    const config = {
        method: 'get',
        url: getAllTeamsRoute,
        headers: {
            'Authorization': "Bearer " + sessionStorage.getItem("token"),
            'Content-Type': 'application/json'
        }
      }
  
    await axios(config)
      .then(response => {
        result = response.data.data
      })
      .catch(error => {
        console.log(error)
      });
  
    return result
}

export const getAllUserByManagerId = async (managerId) => {
    let result = []
  
    const config = {
        method: 'get',
        url: getAllUserByManagerIdRoute + managerId,
        headers: {
            'Authorization': "Bearer " + sessionStorage.getItem("token"),
            'Content-Type': 'application/json'
        }
      }
  
    await axios(config)
      .then(response => {
        result = response.data.data
      })
      .catch(error => {
        console.log(error)
      });
  
    return result
}

export const addNewUserToTeam = async (userId, managerId) => {
  let result = []

  const config = {
    method: 'post',
    url: postAddNewUserToTeamRoute,
    headers: {
      'Authorization': "Bearer " + sessionStorage.getItem("token"),
      'Content-Type': 'application/json'
    },
    data: JSON.stringify({
      "team": {
          "manager": managerId,
          "user": userId
      }
    })
  };

  await axios(config)
    .then(response => {
      result = response.status === 201
    })
    .catch(error => {
      console.log(error)
    });

  return result
}

export const deleteUserFromTeam = async (teamId) => {
  let result = []

  const config = {
    method: 'delete',
    url: deleteUserToTeamRoute + teamId,
    headers: {
      'Authorization': "Bearer " + sessionStorage.getItem("token"),
      'Content-Type': 'application/json'
    }
  };

  await axios(config)
    .then(response => {
      result = response.status === 204
    })
    .catch(error => {
      console.log(error)
    });

  return result
}

export const callBackEndTeams = {
	getAllTeams,
	getAllUserByManagerId,
  addNewUserToTeam,
  deleteUserFromTeam
}
