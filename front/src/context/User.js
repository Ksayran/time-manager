import {
  //getUserByCredentials,
  getAllUsersRoute,
  getUserByUserId,
  postUserWithForm,
  putUserByUserId,
  deleteUserByUserId,
  getUserManager
} from './Config'

import axios from 'axios';

export const getAllUsers = async () =>{
  let result = []

  const config = {
		method: 'get',
		url: getAllUsersRoute,
		headers: {
      'Authorization': "Bearer " + sessionStorage.getItem("token"),
			'Content-Type': 'application/json'
		}
	}

  await axios(config)
    .then(response => {
      result = response.data.data
    })
    .catch(error => {
      console.log(error)
    });

  return result
}

const getUser = async (userId) => {
  let result = []

  const config = {
		method: 'get',
		url: getUserByUserId + userId,
		headers: {
      'Authorization': "Bearer " + sessionStorage.getItem("token"),
			'Content-Type': 'application/json'
		}
	}

  await axios(config)
    .then(response => {
        localStorage.setItem('user', JSON.stringify(response.data.data));
        result = response.data.data;
    })
    .catch(error => {
      console.log(error)
    });

  return result
}

const getUserManagerId = async (userId) => {
  let result = []

  const config = {
		method: 'get',
		url: getUserManager + userId,
		headers: {
      'Authorization': "Bearer " + sessionStorage.getItem("token"),
			'Content-Type': 'application/json'
		}
	}

  await axios(config)
    .then(response => {
      result = response.data.data[0]
    })
    .catch(error => {
      console.log(error)
    });

  return result
}

const postUser = async (user) => {
  let result = []

  const config = {
    method: 'post',
    url: postUserWithForm,
    headers: {
      'Authorization': "Bearer " + sessionStorage.getItem("token"),
      'Content-Type': 'application/json'
    },
    data: JSON.stringify({
      "username": user["username"],
      "email": user["email"],
      "password": user["password"],
      "lastname": user["lastname"],
      "firstname": user["firstname"],
      "role": user["roleId"]
    })
  };

  await axios(config)
    .then(response => {
      result = response.status === 201
    })
    .catch(error => {
      console.log(error)
    });

  return result
}

const deleteUser = async (userId) => {
  let result = []

  const config = {
		method: 'delete',
		url: deleteUserByUserId + userId,
		headers: {
      'Authorization': "Bearer " + sessionStorage.getItem("token"),
			'Content-Type': 'application/json'
		}
	}

  await axios(config)
    .then(response => {
      result = response.status === 204
    })
    .catch(error => {
      console.log(error)
    });

  return result
}

const updateUser = async (user) => {
  let result = []

  const config = {
    method: 'put',
    url: putUserByUserId + user["id"],
    headers: {
      'Authorization': "Bearer " + sessionStorage.getItem("token"),
      'Content-Type': 'application/json'
    },
    data: JSON.stringify({
      "user": {
        "role": user["roleId"], 
        "email": user["email"], 
        "firstname": user["firstname"] , 
        "lastname": user["lastname"]
      }
    })
  };

  await axios(config)
    .then(response => {
      result = response.data
    })
    .catch(error => {
      console.log(error)
    });

  return result
}

export const callBackEnd = {
  getAllUsers,
  getUser,
  postUser,
  deleteUser,
  updateUser,
  getUserManagerId
}