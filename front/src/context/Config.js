//Clocking
export const getClocksByUserId = `http://timemanager.h.metrone.fr:4000/api/clocks/`
export const postClocksByUserId = `http://timemanager.h.metrone.fr:4000/api/clocks/`

//User
export const getUserByCredentials = 'http://timemanager.h.metrone.fr:4000/api/users?'
export const getUserByUserId = 'http://timemanager.h.metrone.fr:4000/api/users/'
export const postUserWithForm = 'http://timemanager.h.metrone.fr:4000/api/users'
export const putUserByUserId = 'http://timemanager.h.metrone.fr:4000/api/users/'
export const deleteUserByUserId = 'http://timemanager.h.metrone.fr:4000/api/users/'
export const getAllUsersRoute = "http://timemanager.h.metrone.fr:4000/api/users"

//WorkingTimes
export const getWorkingTimesByUserId = "http://timemanager.h.metrone.fr:4000/api/workingtimes/";
export const postWorkingTimeByUserId = "http://timemanager.h.metrone.fr:4000/api/workingtimes/";
export const putWorkingTime = "http://timemanager.h.metrone.fr:4000/api/workingtimes/";
export const deleteWorkingTimeById = "http://timemanager.h.metrone.fr:4000/api/workingtimes/";

//Login
export const getApiLogin = "http://timemanager.h.metrone.fr:4000/api/signin";
export const getApiRegister = "http://timemanager.h.metrone.fr:4000/api/users";
export const getLogoutUser = "http://timemanager.h.metrone.fr:4000/api/logout";

// alert
export const getAlertBetweenTwoDates = "http://timemanager.h.metrone.fr:4000/api/workingtimes/alert/"

// manager
export const getUserManager = "http://timemanager.h.metrone.fr:4000/api/teams/user/"

// roles
export const getAllRolesRoute = "http://timemanager.h.metrone.fr:4000/api/roles"

// teams
export const getAllTeamsRoute = "http://timemanager.h.metrone.fr:4000/api/teams"
export const getAllUserByManagerIdRoute = "http://timemanager.h.metrone.fr:4000/api/teams/manager/"
export const postAddNewUserToTeamRoute = "http://timemanager.h.metrone.fr:4000/api/teams"
export const deleteUserToTeamRoute = "http://timemanager.h.metrone.fr:4000/api/teams/"
