import { 
	getClocksByUserId,
	postClocksByUserId
} from './Config'
import jwt from 'jsonwebtoken'
import axios from 'axios';

const getClocks = async () => {
	let result = []
	const token = sessionStorage.getItem("token")
    var decoded = jwt.decode(token);

	const config = {
		method: 'get',
		url: getClocksByUserId + decoded.sub,
		headers: {
			'Authorization': "Bearer " + sessionStorage.getItem("token"),
			'Content-Type': 'application/json'
		}
	}

	await axios(config)
	.then(response => { 
		result = response.data.data
	})
	.catch(error => {
		console.log(error)
	});

	return result
}


const postClocks = async (date) => {
	let result = []
	const token = sessionStorage.getItem("token")
    var decoded = jwt.decode(token);

	var data = JSON.stringify({
		"timer": {
			"time": date
		}
	});
	var config = {
		method: 'post',
		url: postClocksByUserId + decoded.sub,
		headers: { 
			'Authorization': "Bearer " + sessionStorage.getItem("token"),
			'Content-Type': 'application/json'
		},
		data : data
	};

	await axios(config)
	.then(response => {
		if (localStorage.user) {
			localStorage.setItem('clocks', JSON.stringify(response));
		}
		result = response.data.data;
	})
	.catch(error => {
		console.log(error)
	});

	return result
}

export const callBackEndClock = {
	getClocks,
	postClocks
}
