import axios from 'axios';
import moment from "moment";
import {
	getAlertBetweenTwoDates
} from './Config'
import jwt from 'jsonwebtoken'


const getAlertTwoDates = async () => {
	let result
	const token = sessionStorage.getItem("token")
    var decoded = jwt.decode(token);

	const startOfMonth = moment().startOf('month').format('YYYY-MM-DD hh:mm:ss');
	const endOfMonth = moment().format('YYYY-MM-DD hh:mm:ss');

	const newUrl = getAlertBetweenTwoDates + decoded.sub + "?start="+startOfMonth+"&end="+endOfMonth
	const config = {
		method: 'get',
		url: newUrl,
		headers: {
			'Authorization': "Bearer " + sessionStorage.getItem("token"),
			'Content-Type': 'application/json'
		}
	}

	await axios(config)
	.then(response => { 
		result = (response.data)
	})
	.catch(error => {
		console.log(error)
	});

	return result
}

export const callBackEnd = {
	getAlertTwoDates
}