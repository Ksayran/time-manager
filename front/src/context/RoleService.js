import axios from 'axios';
import {
    getApiLogin,
} from './Config'


class RoleService {
    getPublicContent() {
        return axios.get(getApiLogin + 'all');
    }

    getUserBoard() {
        return axios.get(getApiLogin + 'user', { headers: { Authorization: 'Bearer ' + JSON.parse(localStorage.getItem('user')).accessToken }});
    }

    getAdminBoard() {
        return axios.get(getApiLogin + 'admin', { headers: { Authorization: 'Bearer ' + JSON.parse(localStorage.getItem('user')).accessToken }});
    }
}

export default new RoleService();