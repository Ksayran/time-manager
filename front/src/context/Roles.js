import { 
	getAllRolesRoute
} from './Config'
import axios from 'axios';

export const getAllRoles = async () =>{
    let result = []
  
    const config = {
        method: 'get',
        url: getAllRolesRoute,
        headers: {
            'Authorization': "Bearer " + sessionStorage.getItem("token"),
            'Content-Type': 'application/json'
        }
      }
  
    await axios(config)
      .then(response => {
        result = response.data.data
      })
      .catch(error => {
        console.log(error)
      });
  
    return result
}

export const callBackEndRoles= {
	getAllRoles
}
