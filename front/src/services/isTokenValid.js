import jwt from 'jsonwebtoken'

const isTokenExpired = () => {
    const token = sessionStorage.getItem("token");
    const decoded = jwt.decode(token);

    try {
        const date = new Date(0);
        date.setUTCSeconds(decoded.exp);
        return date.valueOf() > new Date().valueOf();
    } catch (err) {
        return false;
    }
}

export const verif = {
    isTokenExpired
}