# Login

This is the component to display the login component

## Methods

<!-- @vuese:Login:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|showError|This function display the input required after the user click on the button connection and it's failed because some fields is required|input - The DOM element of the input that is required message - The message to display below the inpute required|
|showSuccess|Remove the red border when the field change status|input - the DOM element of the input|
|fetchData|This function is trigger when the user click on the login button, it's store in the session storage the token.|-|

<!-- @vuese:Login:methods:end -->


## Data

<!-- @vuese:Login:data:start -->
|Name|Type|Description|Default|
|---|---|---|---|
|email|`String`|The email send by the user in the form|-|
|password|`String`|The password of the user to connect the user|-|

<!-- @vuese:Login:data:end -->


