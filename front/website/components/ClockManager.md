# ClockManager

This is the component to display the Clock component

## Methods

<!-- @vuese:ClockManager:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|fetchData|This function is trigger when the user click on the clock button, it's send the new datetime to the API.|-|

<!-- @vuese:ClockManager:methods:end -->


## Data

<!-- @vuese:ClockManager:data:start -->
|Name|Type|Description|Default|
|---|---|---|---|
|loading|`Boolean`|The loading user information|true|
|data|`Null`|The data|-|
|status|`Null`|The status of the user if he is working or not|-|
|date|`Null`|The the date|-|
|time|`Null`|The time|-|

<!-- @vuese:ClockManager:data:end -->


