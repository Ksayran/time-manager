# Admin

This is the component to display the admin component

## Methods

<!-- @vuese:Admin:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|showError|This function display the input required after the user click on the button connection and it's failed because some fields is required|input - The DOM element of the input that is required message - The message to display below the inpute required|
|showSuccess|Remove the red border when the field change status|input - the DOM element of the input|
|createNewUser|Send the form to create a user to the API|-|
|showFormNewUser|Event on click create new user, display the form|-|
|loadData|This function load all user from the API|-|
|showInfoUser|EEvent on click edit user load information of the user and display it|userId - the id of the user to show more information|
|deleteUser|onClick the delete button of the user ask in alert if the admin is sure to delete the user and delete it|userId - the Id of the user|
|sendUpdateUser|onClick on update user  send the form to the PUT user in the API|userId - the Id of the user|

<!-- @vuese:Admin:methods:end -->


## Data

<!-- @vuese:Admin:data:start -->
|Name|Type|Description|Default|
|---|---|---|---|
|gridColumns|`Array`|The list of all columns in the table|[Firstname,Lastname,Email,Roles,Tools]|
|gridData|`Array`|Store all data to display in the body of the table|-|
|listRoles|`Array`|Store the list of the roles from the DB|-|
|showEditUser|`Boolean`|store the boolean to display or not the form to edit the user|false|
|showAddUser|`Boolean`|store the boolean to display or not the form to add a new user|false|
|userSelected|`Object`|store the user selected in the table by ths user|-|
|newUser|`Object`|store form of the new user to create|-|

<!-- @vuese:Admin:data:end -->


