# Register

This is the component to display the register component

## Methods

<!-- @vuese:Register:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|showError|This function display the input required after the user click on the button connection and it's failed because some fields is required|input - The DOM element of the input that is required message - The message to display below the inpute required|
|showSuccess|Remove the red border when the field change status|input - the DOM element of the input|
|fetchData|This function is trigger when the user click on the register button, it's send the new user form to the API.|-|

<!-- @vuese:Register:methods:end -->


## Data

<!-- @vuese:Register:data:start -->
|Name|Type|Description|Default|
|---|---|---|---|
|firstname|`String`|The firstname of the new user in the form|-|
|lastname|`String`|The lastname of the new user in the form|-|
|email|`String`|The email of the new user in the form|-|
|password|`String`|The password of the new user in the form|-|
|verifyPassword|`String`|The verification of the password of the new user in the form|-|
|username|`String`|The username of the new user in the form|-|

<!-- @vuese:Register:data:end -->


