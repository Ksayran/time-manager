# Alert

This is the component Alert, it display all alerts if user is working less or more in the month

## Methods

<!-- @vuese:Alert:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|handleCloseAlert|Close the alert|id - the id of the alert|

<!-- @vuese:Alert:methods:end -->


## Data

<!-- @vuese:Alert:data:start -->
|Name|Type|Description|Default|
|---|---|---|---|
|listAlerts|`Array`|A list of alerts|-|

<!-- @vuese:Alert:data:end -->


