# Navbar

## Methods

<!-- @vuese:Navbar:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|onDisplayCollaps|This function is for the mobile version to display or not the navbar|-|
|logoutUser|This function is to logout user from the web app, delete session storage|-|

<!-- @vuese:Navbar:methods:end -->


## Data

<!-- @vuese:Navbar:data:start -->
|Name|Type|Description|Default|
|---|---|---|---|
|firstname|`Null`|The firstname of the current user|-|
|lastname|`Null`|The lastname of the current user|-|
|logout|`Call`|The svg for the logout|-|
|displaycCollaps|`Boolean`|The boolean to display or not the navbar in phone vue|false|
|isAdmin|`Boolean`|The display or not the items in the navbar if current user is admin|false|

<!-- @vuese:Navbar:data:end -->


