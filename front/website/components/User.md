# User

## Methods

<!-- @vuese:User:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|getUser|This function get all information of the current user|-|
|createUser|This function create new user|-|
|updateUser|This function update informations of the current user|-|
|deleteUser|This function delete the current user|-|

<!-- @vuese:User:methods:end -->


