# Index

## Methods

<!-- @vuese:Index:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|loadData|This function load data from db to display in the table|-|
|showInfoTeam|this funtion is trigger when admin click on team edit|-|
|addNewUserToTeam|Add new user to the team selected|-|
|deleteUserFromTeam|This function delete the user from the team selected|teamId - id of the team selected|
|deleteAllTeam|This function delete all user from the team|-|
|showFormNewTeam|This function display the form to create a new team|-|
|onChange|This function display the form to add user|-|
|onSelectAllUsersToNewTeam|This function is trigger when admin selected a user to add to the new team|-|
|createNewTeam|This function send new team information to the API|-|
|deleteUserFromNewTeam|This function delete all user from the new team created|-|

<!-- @vuese:Index:methods:end -->


