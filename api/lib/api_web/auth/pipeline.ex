defmodule ApiWeb.Auth.Pipeline do
  use Guardian.Plug.Pipeline, otp_app: :api,
    module: Api.Auth.Guardian,
    error_handler: ApiWeb.Auth.ErrorHandler

    plug Guardian.Plug.VerifySession, claims: %{"typ" => "access"}
    plug Guardian.Plug.VerifyHeader, claims: %{"typ" => "access"}
    plug Guardian.Plug.EnsureAuthenticated
    plug Guardian.Plug.LoadResource
end
