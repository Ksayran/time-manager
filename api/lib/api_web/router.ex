defmodule ApiWeb.Router do
  use ApiWeb, :router

  pipeline :api do
    plug CORSPlug, origin: ["*"]
    plug :accepts, ["json"]
  end

  pipeline :auth do
    plug ApiWeb.Auth.Pipeline
  end

  scope "/api", ApiWeb do
    pipe_through :api
    #Routes Users
    post "/users", UserController, :create
    post "/signin", UserController, :signin
    post "/verify", UserController, :verify_token
  end

  scope "/api", ApiWeb do
    pipe_through [:api, :auth]
    #Routes Users
    get "/users", UserController, :index
    get "/users/:userID", UserController, :show
    post "/logout", UserController, :logout
    post "/refresh", UserController, :refresh_token
    put "/users/:userId", UserController, :update
    delete "/users/:userId", UserController, :delete

    #Routes Role
    get "/roles", RoleController, :index
    get "/roles/:roleId", RoleController, :show
    post "/roles", RoleController, :create
    put "/roles/:roleId", RoleController, :update
    delete "/roles/:roleId", RoleController, :delete

    #Routes Teams
    get "/teams", TeamController, :index
    get "/teams/:teamId", TeamController, :show
    get "/teams/user/:userId", TeamController, :showUser
    get "/teams/manager/:userId", TeamController, :showManager
    post "/teams", TeamController, :create
    put "/teams/:teamId", TeamController, :update
    delete "/teams/:teamId", TeamController, :delete

    #Routes Workingtimes
    get "/workingtimes/:userId", WorkingtimeController, :index
    get "/workingtimes/:userId", WorkingtimeController, :show
    get "/workingtimes/alert/:userId", WorkingtimeController, :user_alert
    get "/workingtimes/:userId/:id", WorkingtimeController, :user_one_workingtime
    post "/workingtimes/:userId", WorkingtimeController, :create
    put "/workingtimes/:id", WorkingtimeController, :update
    delete "/workingtimes/:id", WorkingtimeController, :delete

    #Routes Clocks
    get "/clocks/:user", ClockController, :show
    post "/clocks/:user", ClockController, :create
  end


  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through [:fetch_session, :protect_from_forgery]
      live_dashboard "/dashboard", metrics: ApiWeb.Telemetry
    end
  end

  # Enables the Swoosh mailbox preview in development.
  #
  # Note that preview only shows emails that were sent by the same
  # node running the Phoenix server.
  if Mix.env() == :dev do
    scope "/dev" do
      pipe_through [:fetch_session, :protect_from_forgery]

      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end
