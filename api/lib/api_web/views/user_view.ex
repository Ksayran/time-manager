defmodule ApiWeb.UserView do
  use ApiWeb, :view
  alias ApiWeb.UserView

  def render("index.json", %{users: users}) do
    %{data: render_many(users, UserView, "user.json")}
  end

  def render("show.json", %{user: user}) do
    %{data: render_one(user, UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    %{
      id: user.id,
      username: user.username,
      email: user.email,
      lastname: user.lastname,
      firstname: user.firstname,
      role: user.role,

    }
  end

  def render("rtoken.json", %{token: token}) do
    %{
      token: token
    }
  end


  def render("token.json", %{user: user, token: token}) do
    %{
      email: user.email,
      token: token
    }
  end

end
