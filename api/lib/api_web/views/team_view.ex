defmodule ApiWeb.TeamView do
  use ApiWeb, :view
  alias ApiWeb.TeamView

  def render("index.json", %{teams: teams}) do
    %{data: render_many(teams, TeamView, "team.json")}
  end

  def render("show.json", %{team: team}) do
    %{data: render_one(team, TeamView, "team.json")}
  end

  def render("team.json", %{team: team}) do
    %{
      id: team.id,
      manager: team.manager,
      user: team.user
    }
  end

  #Render pour la requete inner join avec user (récupère nom/prénom manager)
  def render("teamsUser.json", %{teams: teams}) do
    %{data: render_many(teams, TeamView, "teamUser.json")}
  end
  def render("teamUser.json", %{team: team}) do
    %{
      lastnameManager: team.lastname,
      firstnameManager: team.firstname,
      id: team.id,
      user: team.user,
      manager: team.manager
    }
  end



#Render pour la requete inner join avec user (récupère nom/prénom utilisateur)
  def render("teamsManager.json", %{teams: teams}) do
    %{data: render_many(teams, TeamView, "teamManager.json")}
  end
  def render("teamManager.json", %{team: team}) do
    %{
      lastnameUser: team.lastname,
      firstnameUser: team.firstname,
      id: team.id,
      user: team.user,
      manager: team.manager
    }
  end

end
