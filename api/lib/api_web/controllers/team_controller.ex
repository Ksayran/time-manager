defmodule ApiWeb.TeamController do
  use ApiWeb, :controller

  alias Api.Orga
  alias Api.Orga.Team

  action_fallback ApiWeb.FallbackController

  def index(conn, _params) do
    teams = Orga.list_teams()
    render(conn, "index.json", teams: teams)
  end

  def create(conn, %{"team" => team_params}) do
    with {:ok, %Team{} = team} <- Orga.create_team(team_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.team_path(conn, :show, team))
      |> render("show.json", team: team)
    end
  end

  def show(conn, %{"teamId" => id}) do
    team = Orga.get_team!(id)
    render(conn, "show.json", team: team)
  end

  def showUser(conn, %{"userId" => id}) do
    team = Orga.get_teamsUserId!(id)
    render(conn, "teamsUser.json", teams: team)
  end

  def showManager(conn, %{"userId" => id}) do
    team = Orga.get_teamsManagerId!(id)
    render(conn, "teamsManager.json", teams: team)
  end

  def update(conn, %{"teamId" => id, "team" => team_params}) do
    team = Orga.get_team!(id)

    with {:ok, %Team{} = team} <- Orga.update_team(team, team_params) do
      render(conn, "show.json", team: team)
    end
  end

  def delete(conn, %{"teamId" => id}) do
    team = Orga.get_team!(id)

    with {:ok, %Team{}} <- Orga.delete_team(team) do
      send_resp(conn, :no_content, "")
    end
  end
end
