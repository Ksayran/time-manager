defmodule ApiWeb.WorkingtimeController do
  use ApiWeb, :controller

  alias Api.Workingtimes
  alias Api.Workingtimes.Workingtime

  action_fallback ApiWeb.FallbackController


  def index(conn, %{"start" => start, "end" => fin, "userId" => userId}) do
    workingtimes = Workingtimes.get_workingtimesIndex!(start,fin,userId)
    render(conn, "index.json", workingtimes: workingtimes)
  end
  def user_one_workingtime(conn, %{"userId" => userId, "id" => id}) do
    workingtimes = Workingtimes.get_workingtimesid!(userId, id)
    render(conn, "index.json", workingtimes: workingtimes)
  end

  def create(conn, %{"userId" => userId, "workingtimes" => workingtimes_params}) do
    with {:ok, %Workingtime{} = workingtime} <- Workingtimes.create_workingtime(workingtimes_params["start"], workingtimes_params["end"], userId) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.workingtime_path(conn, :show, workingtime))
      |> render("show.json", workingtime: workingtime)
    end
  end

  def user_alert(conn, %{"userId" => userId, "start" => start, "end" => fin}) do
    workingtimes = Workingtimes.get_userAlert!(userId, start, fin)
    render(conn, "alert.json", workingtime: workingtimes)
  end

  def show(conn, %{"userId" => id}) do
    workingtime = Workingtimes.get_workingtime!(id)
    render(conn, "show.json", workingtime: workingtime)
  end

  def update(conn, %{"id" => id, "workingtimes" => workingtime_params}) do
    workingtime = Workingtimes.get_workingtime!(id)

    with {:ok, %Workingtime{} = workingtime} <- Workingtimes.update_workingtime(workingtime, workingtime_params) do
      render(conn, "show.json", workingtime: workingtime)
    end
  end

  def delete(conn, %{"id" => id}) do
    workingtime = Workingtimes.get_workingtime!(id)

    with {:ok, %Workingtime{}} <- Workingtimes.delete_workingtime(workingtime) do
      send_resp(conn, :no_content, "")
    end
  end
end
