defmodule ApiWeb.RoleController do
  use ApiWeb, :controller

  alias Api.Title
  alias Api.Title.Role

  action_fallback ApiWeb.FallbackController

  def index(conn, _params) do
    roles = Title.list_roles()
    render(conn, "index.json", roles: roles)
  end

  def create(conn, %{"role" => role_params}) do
    with {:ok, %Role{} = role} <- Title.create_role(role_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.role_path(conn, :show, role))
      |> render("show.json", role: role)
    end
  end

  def show(conn, %{"roleId" => id}) do
    role = Title.get_role!(id)
    render(conn, "show.json", role: role)
  end

  def update(conn, %{"roleId" => id, "role" => role_params}) do
    role = Title.get_role!(id)

    with {:ok, %Role{} = role} <- Title.update_role(role, role_params) do
      render(conn, "show.json", role: role)
    end
  end

  def delete(conn, %{"roleId" => id}) do
    role = Title.get_role!(id)

    with {:ok, %Role{}} <- Title.delete_role(role) do
      send_resp(conn, :no_content, "")
    end
  end
end
