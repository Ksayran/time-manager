defmodule ApiWeb.ClockController do
  use ApiWeb, :controller

  alias Api.Timer
  alias Api.Timer.Clock
  alias Api.Workingtimes
  #alias Api.Workingtimes.Workingtime

  action_fallback ApiWeb.FallbackController

  # def index(conn, _params) do
  #   clocks = Timer.list_clocks()
  #   render(conn, "index.json", clocks: clocks)
  # end

  def create(conn, %{"user" => user, "timer" => timer_params}) do
    time = NaiveDateTime.from_iso8601!(timer_params["time"])
    if Timer.get_clocks_with_user(user) do
      timer = Timer.get_clocks_with_user(user)
      if timer.status do
        Workingtimes.update_workingtime_by_userid(user, time)
      else
        Workingtimes.create_workingtime_userid(user, time)
      end
      Timer.update_clock(user, time)
      clock = Timer.get_clocks_with_user(user)
      render(conn, "show.json", clock: clock)
    else
      Workingtimes.create_workingtime_userid(user, time)
      with {:ok, %Clock{} = clock} <- Timer.create_clock(user, time) do
        conn
        |> put_status(:created)
        |> put_resp_header("location", Routes.clock_path(conn, :show, clock))
        |> render("show.json", clock: clock)
      end
    end
  end

  # def showClocksUser(conn, %{"user" => user}) do
  #   clock = Timer.get_clocks_by_user!(user)
  #   render(conn, "index.json", clocks: clock)
  # end

  def show(conn, %{"user" => user}) do
    clock = Timer.get_clocks_with_user(user)
    render(conn, "show.json", clock: clock)
  end

  # def update(conn, %{"id" => id, "clock" => clock_params}) do
  #   clock = Timer.get_clock!(id)

  #   with {:ok, %Clock{} = clock} <- Timer.update_clock(clock, clock_params) do
  #     render(conn, "show.json", clock: clock)
  #   end
  # end

  # def delete(conn, %{"id" => id}) do
  #   clock = Timer.get_clock!(id)

  #   with {:ok, %Clock{}} <- Timer.delete_clock(clock) do
  #     send_resp(conn, :no_content, "")
  #   end
  # end
end
