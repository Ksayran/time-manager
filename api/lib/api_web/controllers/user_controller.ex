defmodule ApiWeb.UserController do
  use ApiWeb, :controller

  alias Api.Accounts
  alias Api.Accounts.User
  alias Api.Workingtimes
  alias Api.Workingtimes.Workingtime
  alias Api.Orga
  alias Api.Orga.Team
  alias Api.Timer
  alias Api.Timer.Clock
  alias Api.Auth.Guardian

  action_fallback ApiWeb.FallbackController

  def index(conn, %{"email" => email, "username" => username}) do
    user = Accounts.get_userinfo!(email,username)
    render(conn, "index.json", users: user)
  end

  def index(conn, _param) do
    users = Accounts.list_users()
    render(conn, "index.json", users: users)
  end

  def create(conn, user_params) do
    with {:ok, %User{} = user} <- Accounts.create_user(user_params),
    {:ok, token, _full_claims} = Guardian.encode_and_sign(user, %{}, ttl: {1, :hour}) do
      conn
      |> put_status(:created)
      |> render("token.json", %{user: user, token: token})
    end
  end

  def signin(conn, %{"email" => email, "password" => password}) do
    with {:ok, user, token} <- Guardian.authenticate(email, password) do
      conn
      |> put_status(:ok)
      |> render("token.json", %{user: user, token: token})
    end
  end

  def logout(conn, %{"token" => token}) do
    with {:ok, _} <- Guardian.revoke(token) do
      conn
      |> put_status(:ok)
      |> render("rtoken.json", %{token: "revoke"})
    end
  end

  def refresh_token(conn, %{"token" => token}) do
    with {:ok, _oldtoken, newtoken} <- Guardian.refresh(token) do
      conn
      |> put_status(:ok)
      |> render("rtoken.json", %{token: newtoken})
    end
  end

  def verify_token(conn, %{"token" => token}) do
    with {:ok, _claim} <- Guardian.decode_and_verify(token) do
      conn
      |> put_status(:ok)
      |> render("rtoken.json", %{token: "ok"})
    end
    with {:error, _claim} <- Guardian.decode_and_verify(token) do
      conn
      |> put_status(:unauthorized)
      |> render("rtoken.json", %{token: "nok"})
    end
  end


  def show(conn, %{"userID" => id}) do
    user = Accounts.get_userid!(id)
    render(conn, "show.json", user: user)
  end

  def update(conn, %{"userId" => id, "user" => user_params}) do
    user = Accounts.get_user!(id)

    with {:ok, %User{} = user} <- Accounts.update_user(user, user_params) do
      render(conn, "show.json", user: user)
    end
  end

  def delete(conn, %{"userId" => id}) do
    user = Accounts.get_user!(id)

    if Workingtimes.get_workingtimesUserId!(id) do
      workingtimes = Workingtimes.get_workingtimesUserId!(id)
      for workingtime <- workingtimes do
        with {:ok, %Workingtime{}} <- Workingtimes.delete_workingtime(workingtime) do
          #send_resp(conn, :no_content, "")
        end
      end
    end

    if Orga.get_teamsUserId!(id) do
      teams = Orga.get_teamsUserId!(id)
      for team <- teams do
        with {:ok, %Team{}} <- Orga.delete_team(team) do
          #send_resp(conn, :no_content, "")
        end
      end
    end

    if Timer.get_clocks_with_user(id) do
      timer = Timer.get_clocks_with_user(id)
      with {:ok, %Clock{}} <- Timer.delete_clock(timer) do
        #send_resp(conn, :no_content, "")
      end
    end


    with {:ok, %User{}} <- Accounts.delete_user(user) do
      send_resp(conn, :no_content, "")
    end
  end
end
