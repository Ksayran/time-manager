defmodule Api.Gesparam.Gesparam do
  use Ecto.Schema
  import Ecto.Changeset

  schema "gesparam" do
    field :name, :string
    field :value, :string

    timestamps()
  end

  @doc false
  def changeset(gesparam, attrs) do
    gesparam
    |> cast(attrs, [:name, :value])
    |> validate_required([:name, :value])
  end
end
