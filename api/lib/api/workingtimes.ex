defmodule Api.Workingtimes do
  @moduledoc """
  The Workingtimes context.
  """

  import Ecto.Query, warn: false
  alias Api.Repo
  alias Timex
  alias Api.Gesparam
  alias Api.Workingtimes.Workingtime


  @doc """
  Returns the list of workingtimes.

  ## Examples

      iex> list_workingtimes()
      [%Workingtime{}, ...]

  """
  def list_workingtimes do
    Repo.all(Workingtime)
  end

  @doc """
  Gets a single workingtime.

  Raises `Ecto.NoResultsError` if the Workingtime does not exist.

  ## Examples

      iex> get_workingtime!(123)
      %Workingtime{}

      iex> get_workingtime!(456)
      ** (Ecto.NoResultsError)

  """
  def get_workingtime!(id), do: Repo.get!(Workingtime, id)

  def get_workingtimesIndex!(start,fin,userID) do
    query = from workingtimes in Workingtime,
    where: workingtimes.start >= ^start and workingtimes.end <= ^fin and workingtimes.user == ^userID,
    select: workingtimes
    Repo.all(query)
  end
  def get_workingtimesid!(userId, id) do
    query = from workingtimes in Workingtime,
    where: workingtimes.id == ^id and workingtimes.user == ^userId,
    select: workingtimes
    Repo.all(query)
  end

  def get_workingtimesUserId!(userId) do
    query = from workingtimes in Workingtime,
    where: workingtimes.user == ^userId,
    select: workingtimes
    Repo.all(query)
  end


  #Gestion alerte heure supp
  def get_userAlert!(userId, start, fin) do
    #récupération du temps travaillé durant la période
    start = NaiveDateTime.from_iso8601!(start)
    fin = NaiveDateTime.from_iso8601!(fin)
    query = from workingtimes in Workingtime,
    where: workingtimes.user == ^userId and workingtimes.end <= ^fin and workingtimes.start >= ^start,
    select: %{time: sum(workingtimes.end - workingtimes.start)}
    workingtime = Repo.one(query)

    #calcul nombre nombre jours théorique à partir de start et end
    day = Timex.diff(fin, start, :days)
    day = day - (trunc(day / 7)*2 )

    #récupération du temps normal de travail dans gesparam
    maxworkingtime = Gesparam.get_gesparam!("Workingtime")

    #calcul du nombre d'heure sup
    hour = String.to_float(maxworkingtime.value)
    IO.inspect(workingtime)
    if workingtime.time do
      %{type: "HourOvertime", value: (workingtime.time.secs /3600) - (hour * day)}
    else
      %{type: "HourOvertime", value: 0 - (hour * day)}
    end
  end



  @doc """
  Creates a workingtime.

  ## Examples

      iex> create_workingtime(%{field: value})
      {:ok, %Workingtime{}}

      iex> create_workingtime(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_workingtime(start, fin, user) do
    Repo.insert(%Workingtime {start: NaiveDateTime.from_iso8601!(start), user: String.to_integer(user), end: NaiveDateTime.from_iso8601!(fin)})
  end

  # def create_workingtimes(start, fin, user) do
  #   time  = NaiveDateTime.truncate(NaiveDateTime.local_now(), :second)

  #   Repo.insert(%Workingtimes {start: time, user: String.to_integer(user), end: time})
  # end

  @doc """
  Updates a workingtime.

  ## Examples

      iex> update_workingtime(workingtime, %{field: new_value})
      {:ok, %Workingtime{}}

      iex> update_workingtime(workingtime, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_workingtime(%Workingtime{} = workingtime, attrs) do
    workingtime
    |> Workingtime.changeset(attrs)
    |> Repo.update()
  end

  def update_workingtime_by_userid(userId, time) do
    query = Ecto.Query.from(e in Workingtime, where: e.user == ^userId, order_by: [desc: e.inserted_at], limit: 1)
    most_recent = Repo.one(query)
    from(work in Workingtime, where: work.id == ^most_recent.id,  update: [set: [end: ^time]])
    |> Repo.update_all([])
  end

  def create_workingtime_userid(user, time) do
    Repo.insert(%Workingtime {start: time, user: String.to_integer(user), end: time})
  end

  @doc """
  Deletes a workingtime.

  ## Examples

      iex> delete_workingtime(workingtime)
      {:ok, %Workingtime{}}

      iex> delete_workingtime(workingtime)
      {:error, %Ecto.Changeset{}}

  """
  def delete_workingtime(%Workingtime{} = workingtime) do
    Repo.delete(workingtime)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking workingtime changes.

  ## Examples

      iex> change_workingtime(workingtime)
      %Ecto.Changeset{data: %Workingtime{}}

  """
  def change_workingtime(%Workingtime{} = workingtime, attrs \\ %{}) do
    Workingtime.changeset(workingtime, attrs)
  end
end
