defmodule Api.Gesparam do
  @moduledoc """
  The Timer context.
  """

  import Ecto.Query, warn: false
  alias Api.Repo
  alias Api.Gesparam.Gesparam

  def get_gesparam!(type) do
    query = from gesparam in Gesparam,
    where: gesparam.name == ^type,
    select: gesparam
    Repo.one(query)
  end

end
