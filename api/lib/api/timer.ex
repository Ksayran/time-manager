defmodule Api.Timer do
  @moduledoc """
  The Timer context.
  """

  import Ecto.Query, warn: false
  alias Api.Repo

  alias Api.Timer.Clock

  def list_clocks do
    Repo.all(Clock)
  end

  def get_clock!(id), do: Repo.get!(Clock, id)

  # def get_clocks_by_user!(userId) do
  #   query = from clock in Clock,
  #   where: clock.user == ^userId,
  #   select: clock
  #   Repo.all(query)
  # end

  def get_clocks_with_user(userId) do
    query = from(a in Clock, where: a.user == ^userId)
    Repo.one(query)
  end

  def create_clock(userId, time) do
    Repo.insert(%Clock {time: time, user: String.to_integer(userId), status: true})
  end

  # def update_clock(%Clock{} = clock, attrs) do
  #   clock
  #   |> Clock.changeset(attrs)
  #   |> Repo.update()
  # end

  def update_clock(userId, time) do
    from(p in Clock, where: p.user == ^userId, update: [set: [status: not (p.status), time: ^time]])
    |> Repo.update_all([])
  end

  def delete_clock(%Clock{} = clock) do
    Repo.delete(clock)
  end

  def change_clock(%Clock{} = clock, attrs \\ %{}) do
    Clock.changeset(clock, attrs)
  end
end
