defmodule Api.Orga.Team do
  use Ecto.Schema
  import Ecto.Changeset

  schema "teams" do
    field :manager, :id
    field :user, :id

    timestamps()
  end

  @doc false
  def changeset(team, attrs) do
    team
    |> cast(attrs, [:manager, :user])
    |> validate_required([:manager, :user])
  end
end
