defmodule Api.WorkingtimesTest do
  use Api.DataCase

  alias Api.Workingtimes

  describe "workingtimes" do
    alias Api.Workingtimes.Workingtime
    import Api.AccountsFixtures
    import Api.WorkingtimesFixtures

    @invalid_attrs %{end: nil, start: nil, user: nil}

    test "create_workingtime/1 with valid data creates a workingtime" do
      user = user_fixture()
      time  = "2021-10-27 12:12:00"


      assert {:ok, %Workingtime{} = workingtime} = Workingtimes.create_workingtime(time,time,Integer.to_string(user.id))
      assert workingtime.end == NaiveDateTime.from_iso8601!(time)
      assert workingtime.start == NaiveDateTime.from_iso8601!(time)
      assert workingtime.user == user.id
    end

    test "list_workingtimes/0 returns all workingtimes" do
      workingtime = workingtime_fixture()
      assert Workingtimes.list_workingtimes() == [workingtime]
    end

    test "get_workingtime!/1 returns the workingtime with given id" do
      workingtime = workingtime_fixture()
      assert Workingtimes.get_workingtime!(workingtime.id) == workingtime
    end


    # test "create_workingtime/1 with invalid data returns error changeset" do
    #   assert {:error, %Ecto.Changeset{}} = Workingtimes.create_workingtime(nil, nil, nil)
    # end

    test "update_workingtime/2 with valid data updates the workingtime" do
      workingtime = workingtime_fixture()
      user = user_fixturebis()
      update_attrs = %{end: ~N[2021-10-27 12:12:00], start: ~N[2021-10-27 12:12:00], user: user.id}

      assert {:ok, %Workingtime{} = workingtime} = Workingtimes.update_workingtime(workingtime, update_attrs)
      assert workingtime.end == ~N[2021-10-27 12:12:00]
      assert workingtime.start == ~N[2021-10-27 12:12:00]
      assert workingtime.user == user.id
    end

    test "update_workingtime/2 with invalid data returns error changeset" do
      workingtime = workingtime_fixture()
      assert {:error, %Ecto.Changeset{}} = Workingtimes.update_workingtime(workingtime, @invalid_attrs)
      assert workingtime == Workingtimes.get_workingtime!(workingtime.id)
    end

    test "delete_workingtime/1 deletes the workingtime" do
      workingtime = workingtime_fixture()
      assert {:ok, %Workingtime{}} = Workingtimes.delete_workingtime(workingtime)
      assert_raise Ecto.NoResultsError, fn -> Workingtimes.get_workingtime!(workingtime.id) end
    end

    test "change_workingtime/1 returns a workingtime changeset" do
      workingtime = workingtime_fixture()
      assert %Ecto.Changeset{} = Workingtimes.change_workingtime(workingtime)
    end
  end
end
