defmodule Api.AccountsTest do
  use Api.DataCase

  alias Api.Accounts

  describe "users" do
    alias Api.Accounts.User

    import Api.TitleFixtures
    import Api.AccountsFixtures

    @invalid_attrs_user %{username: nil, email: nil, password: nil, lastname: nil, firstname: nil, role: nil}


    test "create_user/1 with valid data creates a user" do
      role = role_fixture()
      valid_attrs = %{username: "test", email: "test@test.fr", password: "Password", lastname: "test", firstname: "test", role: role.id}

      assert {:ok, %User{} = user} = Accounts.create_user(valid_attrs)
      assert user.username == "test"
      assert user.email == "test@test.fr"
      assert user.lastname == "test"
      assert user.firstname == "test"
    end

    # test "list_users/0 returns all users" do
    #   user = user_fixture()
    #   assert Accounts.list_users() == [user]
    # end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Accounts.get_user!(user.id) == user
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(@invalid_attrs_user)
    end

    test "update_user/2 with valid data updates the user" do
      role = role_fixture()
      user = user_fixture()
      update_attrs = %{username: "test2", email: "test@test.fr", password: "Password", lastname: "test", firstname: "test", role: role.id}

      assert {:ok, %User{} = user} = Accounts.update_user(user, update_attrs)
      assert user.username == "test2"
      assert user.email == "test@test.fr"
      assert user.lastname == "test"
      assert user.firstname == "test"
      assert user.role == role.id
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_user(user, @invalid_attrs_user)
      assert user == Accounts.get_user!(user.id)
    end


    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Accounts.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
     user = user_fixture()
     assert %Ecto.Changeset{} = Accounts.change_user(user)
    end
  end
end
