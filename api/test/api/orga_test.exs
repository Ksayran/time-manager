defmodule Api.OrgaTest do
  use Api.DataCase

  alias Api.Orga

  describe "teams" do
    alias Api.Orga.Team

    import Api.OrgaFixtures
    import Api.AccountsFixtures

    @invalid_attrs %{user: nil, manager: nil}

    test "create_team/1 with valid data creates a team" do
      user = user_fixture()
      valid_attrs = %{user: user.id, manager: user.id}

      assert {:ok, %Team{} = team} = Orga.create_team(valid_attrs)
      assert team.user == user.id
      assert team.manager == user.id
    end

    test "list_teams/0 returns all teams" do
      team = team_fixture()
      assert Orga.list_teams() == [team]
    end

    test "get_team!/1 returns the team with given id" do
      team = team_fixture()
      assert Orga.get_team!(team.id) == team
    end

    test "create_team/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Orga.create_team(@invalid_attrs)
    end

    test "update_team/2 with valid data updates the team" do
      team = team_fixture()
      user = user_fixturebis()
      update_attrs = %{user: user.id, manager: user.id}

      assert {:ok, %Team{} = team} = Orga.update_team(team, update_attrs)
      assert team.user == user.id
      assert team.manager == user.id
    end

    test "update_team/2 with invalid data returns error changeset" do
      team = team_fixture()
      assert {:error, %Ecto.Changeset{}} = Orga.update_team(team, @invalid_attrs)
      assert team == Orga.get_team!(team.id)
    end

    test "delete_team/1 deletes the team" do
      team = team_fixture()
      assert {:ok, %Team{}} = Orga.delete_team(team)
      assert_raise Ecto.NoResultsError, fn -> Orga.get_team!(team.id) end
    end

    test "change_team/1 returns a team changeset" do
      team = team_fixture()
      assert %Ecto.Changeset{} = Orga.change_team(team)
    end
  end
end
