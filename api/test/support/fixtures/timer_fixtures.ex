defmodule Api.TimerFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Api.Timer` context.
  """

  @doc """
  Generate a clock.
  """
  # def clock_fixture(attrs \\ %{}) do
  #   {:ok, clock} =
  #     attrs
  #     |> Enum.into(%{
  #       status: true,
  #       time: ~N[2021-10-25 07:46:00]
  #     })
  #     |> Api.Timer.create_clock()

  #   clock
  # end

  # @doc """
  # Generate a clock.
  # """
  # def clock_fixture(attrs \\ %{}) do
  #   {:ok, clock} =
  #     attrs
  #     |> Enum.into(%{
  #       status: true,
  #       time: ~N[2021-10-25 08:59:00]
  #     })
  #     |> Api.Timer.create_clock()

  #   clock
  # end
end
