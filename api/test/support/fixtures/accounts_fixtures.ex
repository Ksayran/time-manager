defmodule Api.AccountsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Api.Accounts` context.
  """

  @doc """
  Generate a user.
  """
  def user_fixture(attrs \\ %{}) do
    import Api.TitleFixtures
    role = role_fixture()

    {:ok, user} =
      attrs
      |> Enum.into(%{
        username: "test",
        email: "test1@test.fr",
        password: "Password",
        lastname: "test",
        firstname: "test",
        role: role.id
      })
      |> Api.Accounts.create_user()

    user
  end
  def user_fixturebis(attrs \\ %{}) do
    import Api.TitleFixtures
    role = role_fixture()

    {:ok, user} =
      attrs
      |> Enum.into(%{
        username: "test",
        email: "test2@test.fr",
        password: "Password",
        lastname: "test",
        firstname: "test",
        role: role.id
      })
      |> Api.Accounts.create_user()

    user
  end
end
