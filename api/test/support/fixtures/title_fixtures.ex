defmodule Api.TitleFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Api.Title` context.
  """

  @doc """
  Generate a role.
  """
  def role_fixture(attrs \\ %{}) do
    {:ok, role} =
      attrs
      |> Enum.into(%{
        name: "Defaut"
      })
      |> Api.Title.create_role()

    role
  end
end
