defmodule Api.OrgaFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Api.Orga` context.
  """

  @doc """
  Generate a team.
  """
  def team_fixture(attrs \\ %{}) do
    import Api.AccountsFixtures
    user = user_fixture()

    {:ok, team} =
      attrs
      |> Enum.into(%{
        user: user.id,
        manager: user.id
      })
      |> Api.Orga.create_team()

    team
  end
end
