defmodule Api.WorkingtimesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Api.Workingtimes` context.
  """

  @doc """
  Generate a workingtime.
  """
  def workingtime_fixture() do
    import Api.AccountsFixtures
    user = user_fixture()
    time  = "2021-10-27 12:12:00"

    {:ok, workingtime} = Api.Workingtimes.create_workingtime(time,time, Integer.to_string(user.id))

    workingtime
  end
end
