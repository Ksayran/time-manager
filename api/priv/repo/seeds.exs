# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Api.Repo.insert!(%Api.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Api.Repo
alias Api.Accounts.User
alias Api.Orga.Team
alias Api.Title.Role
alias Api.Gesparam.Gesparam

#Ajout role
Repo.insert! %Role{
  name: "Admin"
}
Repo.insert! %Role{
  name: "Collaborateur"
}
Repo.insert! %Role{
  name: "Manager"
}
Repo.insert! %Role{
  name: "RH"
}

password = Bcrypt.Base.hash_password("9f735e0df9a1ddc702bf0a1a7b83033f9f7153a00c29de82cedadc9957289b05", Bcrypt.gen_salt(12, true))

#Ajout utilisateur
Repo.insert! %User{
  username: "testUsername1",
  email: "test1@hotmail.fr",
  password: password,#testpassword
  lastname: "testlastname1",
  firstname: "testfirstname",
  role: 2
}
Repo.insert! %User{
  username: "testUsername2",
  email: "test2@hotmail.fr",
  password: password,#testpassword
  lastname: "testlastname2",
  firstname: "testfirstname",
  role: 2
}
Repo.insert! %User{
  username: "testUsername3",
  email: "test3@hotmail.fr",
  password: password,#testpassword
  lastname: "testlastname3",
  firstname: "testfirstname",
  role: 2
}
Repo.insert! %User{
  username: "TestManager",
  email: "test4@hotmail.fr",
  password: password,#testpassword
  lastname: "TestManager",
  firstname: "TestManager",
  role: 3
}
Repo.insert! %User{
  username: "TestAdmin",
  email: "test5@hotmail.fr",
  password: password, #testpassword
  lastname: "TestAdmin",
  firstname: "TestAdmin",
  role: 1
}


#Création des teams
Repo.insert! %Team{
  manager: 4,
  user: 1
}
Repo.insert! %Team{
  manager: 4,
  user: 2
}
Repo.insert! %Team{
  manager: 4,
  user: 3
}

#Création des gesparam
Repo.insert! %Gesparam{
  name: "StartDay",
  value: "7:00"
}
Repo.insert! %Gesparam{
  name: "EndDay",
  value: "21:00"
}
Repo.insert! %Gesparam{
  name: "Workingtime",
  value: "7.0"
}
Repo.insert! %Gesparam{
  name: "TravelBonus",
  value: "50"
}
Repo.insert! %Gesparam{
  name: "MaxHourOvertime",
  value: "1:00"
}
