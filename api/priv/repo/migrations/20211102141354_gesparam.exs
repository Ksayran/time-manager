defmodule Api.Repo.Migrations.Gesparam do
  use Ecto.Migration

  def change do
    create table(:gesparam) do
      add :name, :string
      add :value, :string

      timestamps()
    end

  end
end
