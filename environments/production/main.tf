terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.13.0"
    }
  }
}

provider "docker" {
  host = "tcp://timemanager.h.metrone.fr:5000"
}

data "docker_registry_image" "postgres" {
  name = "postgres:latest"
}

resource "docker_image" "postgres" {
  name          = data.docker_registry_image.postgres.name
  pull_triggers = [data.docker_registry_image.postgres.sha256_digest]
  keep_locally = false
}

resource "docker_volume" "postgres_volume" {
  name = "postgres_volume"
}

resource "docker_container" "postgres" {
  image = docker_image.postgres.latest
  name = "postgres"
  hostname = "db"
  env = ["POSTGRES_USER=postgres", "POSTGRES_PASSWORD=postgres", "POSTGRES_DB=TimeManager", "POSTGRES_HOST=db"]
  remove_volumes = false
  volumes {
	volume_name = "postgres_volume"
	container_path = "/var/lib/postgresql/data"
  }
  ports {
    internal = 5432
	external = 5432
  }
}

data "docker_registry_image" "front" {
  name = "winterex/time-manager_front:latest"
}

resource "docker_image" "front" {
  name          = data.docker_registry_image.front.name
  pull_triggers = [data.docker_registry_image.front.sha256_digest]
  keep_locally = false
}

resource "docker_container" "front" {
  image = docker_image.front.latest
  name = "front"
  hostname = "front"
  ports {
    internal = 8080
    external = 80
  }
}

data "docker_registry_image" "api" {
  name = "winterex/api-elexir:latest"
}

resource "docker_image" "api" {
  name          = data.docker_registry_image.api.name
  pull_triggers = [data.docker_registry_image.api.sha256_digest]
  keep_locally = false
}

resource "docker_container" "api" {
  image = docker_image.api.latest
  name = "api"
  hostname = "api"
  ports {
    internal = 4000
    external = 4000
  }
}

resource "docker_volume" "sonar_data" {
  name = "sonar_data"
}
resource "docker_volume" "sonar_ext" {
  name = "sonar_ext"
}


resource "docker_image" "sonar" {
  name         = "sonarqube:latest"
  keep_locally = false
}

resource "docker_container" "sonar" {
  image = docker_image.sonar.latest
  name = "sonar"
  hostname = "sonar"
  volumes {
	volume_name = "sonar_ext"
	container_path = "/opt/sonarqube/extensions"
  }
  volumes {
	volume_name = "sonar_data"
	container_path = "/opt/sonarqube/data"
  }
  ports {
    internal = 9000
    external = 9000
  }
  ports {
    internal = 9092
    external = 9092
  }
}
